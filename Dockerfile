# 'alpine:edge' provides the most latest golang.
FROM alpine:edge

ARG user=buildozer

RUN apk --no-cache upgrade ;\
    apk --no-cache add alpine-sdk go libcap bash ;\
    adduser -D "$user" ;\
    adduser "$user" abuild ;\
    echo "$user ALL=(ALL) NOPASSWD: ALL" >>/etc/sudoers

USER "$user"

WORKDIR /home/"$user"

ADD abuild.conf .abuild/abuild.conf
ADD gitconfig .gitconfig
RUN sudo chown -R $user:$user ~

ENV repos ""

CMD sh -c "test -z $repos || echo \"$repos\" >/etc/apk/repositories ; exec /bin/sh"
